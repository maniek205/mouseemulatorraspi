import uinput
import evdev
import time
from enum import Enum
from evdev import InputDevice, list_devices

import threading
import time
import sys
import Queue

EVENT_PATH = "/dev/input/event0"
CONTROLLER_NAME = "GPIO Controller 1"
def findEventPath():
	devices = map(InputDevice, list_devices())
	for dev in devices:
		if dev.name == CONTROLLER_NAME:
			return dev.fn
	print "ERROR - controler not found"
	exit(1)

EVENT_PATH = findEventPath()
AXIS_VALUE_TYPE = 3

X_CODE = 0
X_LEFT_VALUE = -1
X_RIGHT_VALUE = X_LEFT_VALUE * (-1)

Y_CODE = 1
Y_UP_VALUE = -1
Y_DOWN_VALUE = Y_UP_VALUE * (-1)

BUTTONS_TYPE = 1
X_BUTTON_CODE = 307
Y_BUTTON_CODE = 308
B_BUTTON_CODE = 305
A_BUTTON_CODE = 304

class CONST_BUTTONS(Enum):
	CONST_X_R_ON = 0
	CONST_X_L_ON = 1
	CONST_Y_U_ON = 2
	CONST_Y_D_ON = 3
	CONST_BUTTON_A_ON = 4
	CONST_BUTTON_B_ON = 5
	CONST_BUTTON_X_ON = 6
	CONST_BUTTON_Y_ON = 7
	CONST_TR1_ON = 8
	CONST_TR2_ON = 9
	CONST_X_OFF = 10
	CONST_Y_OFF = 11
	CONST_BUTTON_A_OFF = 12
	CONST_BUTTON_B_OFF = 13
	CONST_BUTTON_X_OFF = 14
	CONST_BUTTON_Y_OFF = 15

class MouseThread(threading.Thread):
	def __init__(self, queue):
		super(MouseThread, self).__init__()
		self.queue = queue
		self.device = uinput.Device([
			uinput.BTN_LEFT,
			uinput.BTN_RIGHT,
			uinput.REL_Y,
			uinput.REL_X,
        ])
		self.button_state = {
			'left' : 0,
			'right' : 0,
			'up' : 0,
			'down' : 0,
			'lmb' : 0,
			'rmb' : 0
		}
		self.localstate = {
			'X_R' : 0,
			'X_L' : 0,
			'Y_U' : 0,
			'Y_D' : 0,
			'BUT_A' : 0,
			'BUT_B' : 0,
			'prevBUTT_A' : 0,
			'prevBUTT_B' : 0,
		}
		self.BASE_SAMPLING = 0.01
		self.samling = self.BASE_SAMPLING
		self.MIN_SAMPLING = 0.002
		self.SUB_SAMPLING = 0.0001
		self.MOVING_SKIP = 10
		self.TIMEOUT = 1
		self.MOVING = 0
		self.MOVE_MOUSE_VALUE=1

		try:
			self.start()
		except (KeyboardInterrupt, SystemExit):
			cleanup_stop_thread()
			sys.exit()
			
	def updatelocalstate(self, value):
		if value == CONST_BUTTONS.CONST_X_OFF:
			self.localstate['X_L'] = 0
			self.localstate['X_R'] = 0
		if value == CONST_BUTTONS.CONST_Y_OFF:
			self.localstate['Y_D'] = 0
			self.localstate['Y_U'] = 0
		if value == CONST_BUTTONS.CONST_X_L_ON:
			self.localstate['X_L'] = 1
		if value == CONST_BUTTONS.CONST_X_R_ON:
			self.localstate['X_R'] = 1
		if value == CONST_BUTTONS.CONST_Y_U_ON:
			self.localstate['Y_U'] = 1
		if value == CONST_BUTTONS.CONST_Y_D_ON:
			self.localstate['Y_D'] = 1
		if value == CONST_BUTTONS.CONST_BUTTON_A_ON:
			self.localstate['BUT_A'] = 1
		if value == CONST_BUTTONS.CONST_BUTTON_B_ON:
			self.localstate['BUT_B'] = 1
		if value == CONST_BUTTONS.CONST_BUTTON_A_OFF:
			self.localstate['BUT_A'] = 0
		if value == CONST_BUTTONS.CONST_BUTTON_B_OFF:
			self.localstate['BUT_B'] = 0

	def incrementMoving(self):
		if self.MOVING <= (self.MOVING_SKIP+2):
			self.MOVING = self.MOVING + 1

	def execute(self):
		if ((self.localstate['X_R'] == 0) & \
			(self.localstate['X_L'] == 0) & \
			(self.localstate['Y_D'] == 0) & \
			(self.localstate['Y_U'] == 0)) :
			self.MOVING = 0
			self.TIMEOUT = 0
		if self.localstate['X_R'] == 1:
			self.incrementMoving()
			self.TIMEOUT = -1
			self.device.emit(uinput.REL_X, self.MOVE_MOUSE_VALUE)
		if self.localstate['X_L'] == 1:
			self.incrementMoving()
			self.TIMEOUT = -1
			self.device.emit(uinput.REL_X, (-1)*self.MOVE_MOUSE_VALUE)
		if self.localstate['Y_U'] == 1:
			self.incrementMoving()
			self.TIMEOUT = -1
			self.device.emit(uinput.REL_Y, (-1)*self.MOVE_MOUSE_VALUE)
		if self.localstate['Y_D'] == 1:
			self.incrementMoving()
			self.TIMEOUT = -1
			self.device.emit(uinput.REL_Y, self.MOVE_MOUSE_VALUE)
		if self.localstate['prevBUTT_A'] != self.localstate['BUT_A']:
			self.device.emit(uinput.BTN_LEFT, self.localstate['BUT_A'])
			self.localstate['prevBUTT_A'] = self.localstate['BUT_A']
		if self.localstate['prevBUTT_B'] != self.localstate['BUT_B']:
			self.device.emit(uinput.BTN_RIGHT, self.localstate['BUT_B'])
			self.localstate['prevBUTT_B'] = self.localstate['BUT_B']

	def updateTimeout(self):
		if self.MOVING > self.MOVING_SKIP:
			if self.samling > self.MIN_SAMPLING:
				self.samling = self.samling - self.SUB_SAMPLING
		else:
			self.samling = self.BASE_SAMPLING


	def run(self):
		value = CONST_BUTTONS.CONST_Y_OFF
		while(1):
			try:
				value = self.queue.get(timeout=self.samling)
				#print value
			except:
				#print "e"
				pass
			self.updatelocalstate(value)
			self.execute()
			self.updateTimeout()
				

class MainThread(threading.Thread):
	def __init__(self):
		super(MainThread, self).__init__()
		#Queue for products
		self.q = Queue.Queue()
		MouseThread(self.q)
		#start
		try: 
			self.start()
		except(KeyboardInterrupt, SystemExit):
			cleanup_stop_thread()
			sys.exit()
		
	def SendValue(self, value):
		self.q.put(value)
		#print value

		
	def run(self):
		for event in joypad.read_loop():
			#print event
			if event.type == AXIS_VALUE_TYPE:
				if event.code == X_CODE:
					if event.value == X_RIGHT_VALUE:
						self.SendValue(CONST_BUTTONS.CONST_X_R_ON)
					elif event.value == X_LEFT_VALUE:
						self.SendValue(CONST_BUTTONS.CONST_X_L_ON)
					else:
						self.SendValue(CONST_BUTTONS.CONST_X_OFF)
				elif event.code == Y_CODE:
					if event.value == Y_UP_VALUE:
						self.SendValue(CONST_BUTTONS.CONST_Y_U_ON)
					elif event.value == Y_DOWN_VALUE:
						self.SendValue(CONST_BUTTONS.CONST_Y_D_ON)
					else:
						self.SendValue(CONST_BUTTONS.CONST_Y_OFF)
				else:
					#print event
					pass

			if event.type == BUTTONS_TYPE:
				if event.code == A_BUTTON_CODE:
					if event.value == 0:
						self.SendValue(CONST_BUTTONS.CONST_BUTTON_A_OFF)
					else:
						self.SendValue(CONST_BUTTONS.CONST_BUTTON_A_ON)
				if event.code == B_BUTTON_CODE:
					if event.value == 0:
						self.SendValue(CONST_BUTTONS.CONST_BUTTON_B_OFF)
					else:
						self.SendValue(CONST_BUTTONS.CONST_BUTTON_B_ON)


joypad = evdev.InputDevice(EVENT_PATH)
joypad.grab()

MainThread()		

